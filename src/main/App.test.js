import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('Renders APP Menu', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/Workday/i);
  expect(linkElement).toBeInTheDocument();
});
