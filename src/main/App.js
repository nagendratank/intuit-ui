import React,{Component,Suspense} from 'react';
import './App.css';
import 'semantic-ui-css/semantic.min.css'
import { Menu, Segment } from 'semantic-ui-react'
import 'react-table-6/react-table.css'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import "antd/dist/antd.css"; // or import "antd/dist/antd.css";
import 'react-awesome-query-builder/lib/css/styles.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useLocation
} from "react-router-dom";

const WorkDayReport = React.lazy(() => import('../components/WorkDayReports'));
const AlightReport = React.lazy(() => import('../components/AlightReport'));
const RulesComponent = React.lazy(() => import('../components/RulesComponent'));
const ResultComponent = React.lazy(() => import('../components/ResultComponent'));
const RuleList = React.lazy(() => import('../components/RuleList'));

export default class App extends Component {
 
  state = { activeItem: '' }

  handleItemClick = (e, { name }) => this.setState({ activeItem: name })

  render() {
   
    const { activeItem } = this.state
    const path = window.location.pathname;
    return (
      <Router>
        <Menu pointing secondary>
          <Menu.Item
            as={Link}
            to="/"
            name='Workday'
            active={activeItem === 'Workday' || window.location.pathname==="/"}
            onClick={this.handleItemClick}
          />
          <Menu.Item
            as={Link}
            to="/alight"
            name='Alight'
            active={activeItem === 'Alight' || window.location.pathname==="/alight"}
            onClick={this.handleItemClick}
          />
          <Menu.Item
           as={Link}
            name='Result'
            to="/result"
            active={activeItem === 'Result' || window.location.pathname==="/result"} 
            onClick={this.handleItemClick}
          />
          <Menu.Menu position='right'>
            <Menu.Item
             as={Link}
              name='Rules'
              to="/rules"
              active={activeItem === 'Rules' || window.location.pathname==="/rules"}
              onClick={this.handleItemClick}
            />
          </Menu.Menu>
        </Menu>
        <ToastContainer />
        <Suspense fallback={<div>Loading...</div>}>
        <Switch>
          <Route exact path="/" component={WorkDayReport}/>
          <Route exact path="/alight" component={AlightReport}/>
          <Route exact path="/result" component={ResultComponent}/>
          <Route exact path="/rules" component={RuleList}/>
          <Route exact path="/createrule" component={RulesComponent}/>
          <Route exact path="/updaterule/:id" component={RulesComponent}/>
        </Switch>
        </Suspense>
      </Router>
    )
  }
}

