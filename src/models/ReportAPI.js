import axios from 'axios';

const axionInstance = axios.create({
    baseURL: (process.env.NODE_ENV ==="development"? 'http://localhost:8080':'http://13.68.103.175:8080'),
    timeout: 10000,
});


export const getReport = (url)=>{
    console.log(process.env);
    return axionInstance.get(url);
}

export const saveReport = (url,formData)=>{
    return axionInstance.post(url, formData, {
        headers: {
        'Content-Type': 'multipart/form-data'
        }
    })
}

export const saveRule = (url,payload)=>{
    return axionInstance.post(url, payload)
}

export const getRule = (url)=>{
    return axionInstance.get(url)
}

export const updateRule = (url,payload)=>{
    return axionInstance.put(url,payload)
}

export const applyRule = (url)=>{
    return axionInstance.get(url);
}

export const deleteRule = (url)=>{
    return axionInstance.delete(url)
}