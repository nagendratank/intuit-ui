import React,{useState,useEffect} from 'react'
import {Link} from "react-router-dom";
import TableComponent from './TableComponent';
import {applyRule,getReport} from '../models/ReportAPI';
import LoadingComponent from './LoadingComponent';
import {Segment,Button, Header, Image, Modal } from 'semantic-ui-react'
import { toast } from 'react-toastify';
import moment from 'moment-timezone';
import {CSVLink, CSVDownload} from 'react-csv';

function ResultComponent() {
  const [loading, setLoading] = useState(true)
  const [data, setData] = useState([])
  
  const fetchData = async () => {
    setLoading(true);
    try {
        const result = await getReport("/reports/alight");
        setData(result.data);
        setLoading(false);
      } catch(err) {
        setLoading(false);
        toast.error("Failed to load rules");
     }
  };



  useEffect(()=>{
      fetchData();
  },[])

  const runRule = () =>{
    setLoading(true);
    applyRule("/rules/run").then((result)=>{
        toast.success("Rule Applied Successfully");
        fetchData();
    })
  }
  
  const columns = React.useMemo(
    () => [
        {
            Header: 'First Name',
            accessor: 'firstName',
          },
          {
            Header: 'Last Name',
            accessor: 'lastName',
          },
          {
            Header: 'Hire Date',
            accessor: 'hireDate',
            Cell: cellInfo => <span>{cellInfo.value?moment.tz(cellInfo.value,"Asia/Kolkata").format("MM/DD/YYYY"):null}</span>
          },{
            Header: 'Termination Date',
            accessor: 'terminationDate',
            Cell: cellInfo => <span>{cellInfo.value?moment.tz(cellInfo.value,"Asia/Kolkata").format("MM/DD/YYYY"):null}</span>
          },
          {
            Header: 'Benefits Code',
            accessor: 'rule.action',
            Cell: cellInfo => {
                return <span>{cellInfo.value || 'Active'}</span>
            },
            backGroundColor: cellInfo => {
                return {style:{backgroundColor:cellInfo?.row?.original?.rule?.color}}
            }
          }
    ],
    []
  )
  
  return (
    <Segment className="segment-class">
         <div style={{"textAlign":"right"}}>
                <Button secondary onClick={runRule}>Apply Rule</Button>
                <Button secondary className="downloadBtn">
                    <CSVLink data={data} filename="result.csv">
                        Download File
                    </CSVLink>
                </Button>
        </div>
      {loading?<LoadingComponent />:<TableComponent columns={columns} data={data} />}
    </Segment>
  )
}

export default ResultComponent


