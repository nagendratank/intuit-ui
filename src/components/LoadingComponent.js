import React from 'react'
import { Dimmer, Loader, Image, Segment } from 'semantic-ui-react'

const LoadingComponent = () => (
      <Dimmer active inverted>
        <Loader inverted>Loading</Loader>
      </Dimmer>
)

export default LoadingComponent