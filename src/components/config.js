import React from "react";
import merge from "lodash/merge";
import { BasicConfig } from "react-awesome-query-builder";
import AntdConfig from "react-awesome-query-builder/lib/config/antd";
const InitialConfig = AntdConfig;


//////////////////////////////////////////////////////////////////////

const fields = {
  workday_report: {
    label: "Workday",
    tooltip: "Group of fields",
    type: "!struct",
    subfields: {
      employment_type: {
        label: "Employment Type", //only for menu's toggler
        type: "select",
        valueSources: ["value"],
        fieldSettings: {
          listValues: [
            { value: "F", title: "Full Time" },
            { value: "P", title: "Part Time" },
          ],
        }
      },
      termination_status: {
        label: "Termination Status", //only for menu's toggler
        type: "boolean"
      },
      termination_date: {
        label: "Termination Date", //only for menu's toggler
        type: "datetime"
      },
      termination_reason: {
        label: "Termination Reason", //only for menu's toggler
        type: "select",
        valueSources: ["value"],
        fieldSettings: {
          // * old format:
          // listValues: {
          //     yellow: 'Yellow',
          //     green: 'Green',
          //     orange: 'Orange'
          // },
          // * new format:
          listValues: [
            { value: "Voluntary>Resignation", title: "Voluntary Resignation" },
            { value: "Death", title: "Death" },
          ],
        }, 
      },
      leave_status: {
        label: "Leave Status", //only for menu's toggler
        type: "boolean"
      },
      leave_type: {
        label: "Leave Types",
        type: "select",
        fieldSettings: {
          listValues: ["Regulatory > Family Medical Leave Act", "Company > Disability", "Personal > Health Reasons","Regulatory > Uncertified Medical Leave","Regulatory > Workers Compensation","Personal > Personal Leave","Regulatory > Work Authorization Leave"],
        },
        valueSources: ["value"],
      }
    }
  }
};


//////////////////////////////////////////////////////////////////////


const conjunctions = {
  AND: InitialConfig.conjunctions.AND,
  OR: InitialConfig.conjunctions.OR,
};


const proximity = {
  ...InitialConfig.operators.proximity,
  valueLabels: [
    { label: "Word 1", placeholder: "Enter first word" },
    { label: "Word 2", placeholder: "Enter second word" },
  ],
  textSeparators: [
    //'Word 1',
    //'Word 2'
  ],
  options: {
    ...InitialConfig.operators.proximity.options,
    optionLabel: "Near", // label on top of "near" selectbox (for config.settings.showLabels==true)
    optionTextBefore: "Near", // label before "near" selectbox (for config.settings.showLabels==false)
    optionPlaceholder: "Select words between", // placeholder for "near" selectbox
    minProximity: 2,
    maxProximity: 10,
    defaults: {
      proximity: 2
    },
    customProps: {}
  }
};

const operators = {
  ...InitialConfig.operators,
  // examples of  overriding
  between: {
    ...InitialConfig.operators.between,
    valueLabels: [
      "Value from",
      "Value to"
    ],
    textSeparators: [
      "from",
      "to"
    ],
  },
  proximity,
};

const widgets = {
  ...InitialConfig.widgets,
  // examples of  overriding
  text: {
    ...InitialConfig.widgets.text,
  },
  slider: {
    ...InitialConfig.widgets.slider,
    customProps: {
      width: "300px"
    }
  },
  rangeslider: {
    ...InitialConfig.widgets.rangeslider,
    customProps: {
      width: "300px"
    }
  },
  date: {
    ...InitialConfig.widgets.date,
    dateFormat: "DD.MM.YYYY",
    valueFormat: "YYYY-MM-DD",
  },
  time: {
    ...InitialConfig.widgets.time,
    timeFormat: "HH:mm",
    valueFormat: "HH:mm:ss",
  },
  datetime: {
    ...InitialConfig.widgets.datetime,
    timeFormat: "HH:mm",
    dateFormat: "DD.MM.YYYY",
    valueFormat: "YYYY-MM-DD HH:mm:ss",
  },
  func: {
    ...InitialConfig.widgets.func,
    customProps: {
      showSearch: true
    }
  },
  treeselect: {
    ...InitialConfig.widgets.treeselect,
    customProps: {
      showSearch: true
    }
  },
};


const types = {
  ...InitialConfig.types,
  // examples of  overriding
  boolean: merge(InitialConfig.types.boolean, {
    widgets: {
      boolean: {
        widgetProps: {
          hideOperator: true,
          operatorInlineLabel: "is",
        }
      },
    },
  }),
};


const localeSettings = {
  locale: {
    short: "ru",
    full: "ru-RU",
  },
  valueLabel: "Value",
  valuePlaceholder: "Value",
  fieldLabel: "Field",
  operatorLabel: "Operator",
  fieldPlaceholder: "Select field",
  operatorPlaceholder: "Select operator",
  deleteLabel: null,
  addGroupLabel: "Add group",
  addRuleLabel: "Add rule",
  delGroupLabel: null,
  notLabel: "Not",
  valueSourcesPopupTitle: "Select value source",
  removeRuleConfirmOptions: {
    title: "Are you sure delete this rule?",
    okText: "Yes",
    okType: "danger",
  },
  removeGroupConfirmOptions: {
    title: "Are you sure delete this group?",
    okText: "Yes",
    okType: "danger",
  },
};

const settings = {
  ...InitialConfig.settings,
  ...localeSettings,

  valueSourcesInfo: {
    value: {
      label: "Value"
    },
    field: {
      label: "Field",
      widget: "field",
    },
    func: {
      label: "Function",
      widget: "func",
    }
  },
  // canReorder: false,
  // canRegroup: false,
  // showNot: false,
  // showLabels: true,
  maxNesting: 3,
  canLeaveEmptyGroup: true, //after deletion
    
  // renderField: (props) => <FieldCascader {...props} />,
  // renderOperator: (props) => <FieldDropdown {...props} />,
  // renderFunc: (props) => <FieldSelect {...props} />,
};


const funcs = {
  LOWER: {
    label: "Lowercase",
    mongoFunc: "$toLower",
    jsonLogic: ({str}) => ({ "method": [ str, "toLowerCase" ] }),
    returnType: "text",
    args: {
      str: {
        label: "String",
        type: "text",
        valueSources: ["value", "field"],
      },
    }
  },
  LINEAR_REGRESSION: {
    label: "Linear regression",
    returnType: "number",
    formatFunc: ({coef, bias, val}, _) => `(${coef} * ${val} + ${bias})`,
    sqlFormatFunc: ({coef, bias, val}) => `(${coef} * ${val} + ${bias})`,
    mongoFormatFunc: ({coef, bias, val}) => ({"$sum": [{"$multiply": [coef, val]}, bias]}),
    jsonLogic: ({coef, bias, val}) => ({ "+": [ {"*": [coef, val]}, bias ] }),
    renderBrackets: ["", ""],
    renderSeps: [" * ", " + "],
    args: {
      coef: {
        label: "Coef",
        type: "number",
        defaultValue: 1,
        valueSources: ["value"],
      },
      val: {
        label: "Value",
        type: "number",
        valueSources: ["value"],
      },
      bias: {
        label: "Bias",
        type: "number",
        defaultValue: 0,
        valueSources: ["value"],
      }
    }
  },
};


const config = {
  conjunctions,
  operators,
  widgets,
  types,
  settings,
  fields,
  funcs
};

export default config;