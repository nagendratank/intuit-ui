import React, {Component} from 'react';
import {Query, Builder, BasicConfig, Utils as QbUtils} from 'react-awesome-query-builder';

import config from './config';
import { Button, Checkbox, Form } from 'semantic-ui-react'
import {saveRule,getRule,updateRule} from '../models/ReportAPI';
import { toast } from 'react-toastify';
import LoadingComponent from './LoadingComponent';
import { withRouter } from "react-router-dom";


// You need to provide your own config. See below 'Config format'
 
// You can load query value from your backend storage (for saving see `Query.onChange()`)
const queryValue = {"id": QbUtils.uuid(), "type": "group"};

class RuleComponent extends Component {
    
    state = {
      tree: QbUtils.checkTree(QbUtils.loadTree(queryValue), config),
      config: config,
      response:{},
      loading:true,
      id:null
    };

    getRuleData = (id)=>{
        getRule("/rules/"+id).then((res)=>{
            this.setState({
                tree: res.data && QbUtils.loadTree(JSON.parse(res.data.tree)),
                loading:false,
                response:res.data,
            },()=>{
                this.ruleNameRef.value = res.data && res.data.ruleName;
                this.actionRef.value =  res.data && res.data.action;
                this.descRef.value = res.data && res.data.description;
                this.conditionRef.value= res.data && JSON.stringify(QbUtils.sqlFormat(this.state.tree, this.state.config))
                this.priorityRef.value = res?.data?.priority;
                this.emailRef.value = res?.data?.email;
            });
        },(err)=>{
            toast.error("Failed to load rule");
        })
    }

    createRule = () =>{
        let payload = {
            "ruleName":this.ruleNameRef.value,
            "condition":this.conditionRef.value,
            "description":this.descRef.value,
            "action":this.actionRef.value,
            "tree":JSON.stringify(this.state.tree),
            "priority": this.priorityRef.value, 
            "email":this.emailRef.value,
            "color":"#bfb000".replace(/0/g,function(){return (~~(Math.random()*16)).toString(16);})
        }
        saveRule('/rules/',payload).then((res)=>{
            toast.success("Rule Saved Successfully");
            this.props.history.push("/rules")
        },(err)=>{
            toast.error("Rule creation failed");
        })
    }

    updateRule = () =>{
        let payload = {
            "ruleId":this.state.response.ruleId,
            "ruleName":this.ruleNameRef.value,
            "condition":this.conditionRef.value,
            "description":this.descRef.value,
            "action":this.actionRef.value,
            "tree":JSON.stringify(this.state.tree),
            "priority": this.priorityRef.value, 
            "email":this.emailRef.value,
            "color":"#bfb000".replace(/0/g,function(){return (~~(Math.random()*16)).toString(16);})
        }
        updateRule('/rules/',payload).then((res)=>{
            toast.success("Rule Updated Successfully");
            this.props.history.push("/rules")
        },(err)=>{
            toast.error("Rule updation failed");
        })
    }


    componentDidMount(){
        this.setState({
            id:this.props.match.params.id
        },()=>{
            let id = this.props.match.params.id;
            if(id){
                this.getRuleData(id);
            }else{
                this.setState({
                    loading:false
                })
            }
        })
        
       

    }

    copyRule = () =>{
        this.setState({
            id:null
        })
    }
     
    
    render = () => (
      this.state.loading?<LoadingComponent/>:
      
      <div>
            {this.state.id && <div style={{"textAlign":"right"}}>
                    <Button secondary onClick={this.copyRule}>Copy from this rule</Button>
                </div>
            }
                <Query
            {...config} 
            value={this.state.tree}
            onChange={this.onChange}
            renderBuilder={this.renderBuilder}
        />
        
        <Form style={{"padding":"0 2rem"}}>
            <Form.Group widths='equal'>
            <Form.Field>
                <label>Rule Name</label>
                <input placeholder='Rule Name'  ref={input => this.ruleNameRef = input}/>
            </Form.Field>
            <Form.Field>
                <label>Priority</label>
                <input placeholder='Rule Name' type="number"  ref={input => this.priorityRef = input}/>
            </Form.Field>
            
            </Form.Group>
           
            <Form.Group widths='equal'>
                <Form.Field>
                    <label>Action</label>
                    <input placeholder='Action'  ref={input => this.actionRef  = input}/>
                </Form.Field>
                <Form.Field>
                    <label>Email</label>
                    <input placeholder='Email'  ref={input => this.emailRef  = input}/>
                </Form.Field>
            </Form.Group>
            <Form.Field>
                <label>Rule Expression</label>
                <textarea placeholder='Expression' ref={input => this.conditionRef  = input }/>
            </Form.Field>

            <Form.Field>
                <label>Description</label>
                <input placeholder='Description' ref={input =>this.descRef = input }/>
            </Form.Field>
           
            {this.state.id?<Button type='submit' onClick={this.updateRule}>Update</Button>:<Button type='submit' onClick={this.createRule}>Save</Button>}
        </Form>
      </div>
    )
 
    renderBuilder = (props) => (
      <div className="query-builder-container" style={{padding: '10px'}}>
        <div className="query-builder qb-lite">
            <Builder {...props} />
        </div>
      </div>
    )
 
    renderResult = ({tree: immutableTree, config}) => (

    
      <div className="query-builder-result">
          <div>Query string: <pre>{JSON.stringify(QbUtils.queryString(immutableTree, config))}</pre></div>
          <div>MongoDb query: <pre>{JSON.stringify(QbUtils.mongodbFormat(immutableTree, config))}</pre></div>
          <div>SQL where: <pre>{JSON.stringify(QbUtils.sqlFormat(immutableTree, config))}</pre></div>
          <div>JsonLogic: <pre>{JSON.stringify(QbUtils.jsonLogicFormat(immutableTree, config))}</pre></div>
      </div>
    )
    
    onChange = (immutableTree, config) => {
      // Tip: for better performance you can apply `throttle` - see `examples/demo`
      this.setState({tree: immutableTree, config: config});
      this.conditionRef.value=JSON.stringify(QbUtils.sqlFormat(immutableTree, config))
      const jsonTree = QbUtils.getTree(immutableTree);
      console.log(jsonTree);
      // `jsonTree` can be saved to backend, and later loaded to `queryValue`
    }
}

export default withRouter(RuleComponent);