import React,{useState,useEffect} from 'react'
import {Link} from "react-router-dom";
import TableComponent from './TableComponent';
import {getRule,deleteRule} from '../models/ReportAPI';
import LoadingComponent from './LoadingComponent';
import {Segment,Button, Header, Image, Modal } from 'semantic-ui-react'
import { toast } from 'react-toastify';


function RuleList() {
  const [loading, setLoading] = useState(true)
  const [data, setData] = useState([])
  
  const fetchData = async () => {
    setLoading(true);
    try {
        const result = await getRule("/rules/");
        setData(result.data);
        setLoading(false);
      } catch(err) {
        setLoading(false);
        toast.error("Failed to load rules");
     }
  };

  function MyCell({ value, columnProps: { rest: { someFunc } } }) {
    return <a href="#" onClick={someFunc}>{value}</a>
  }

  useEffect(()=>{
      fetchData();
  },[])

  const deleteRuleWithId = (ruleId)=>{
    deleteRule('/rules/'+ruleId).then(()=>{
       fetchData();
    },(err)=>{
        toast.error("Failed to delete rule");
    })
  }

  const columns = React.useMemo(
    () => [
        {
            Header: 'Rule Name',
            accessor: 'ruleName',
            Cell: cellInfo => {
                return <Link to={"/updaterule/"+cellInfo.row.original.ruleId}>{cellInfo.value}</Link>
            }
          },
          {
            Header: 'Description',
            accessor: 'description',
          },
          {
            Header: 'Action',
            accessor: 'action',
          },
          {
            Header: 'Condition',
            accessor: 'condition',
          },
          {
            Header: 'Priority',
            accessor: 'priority',
          },
          {
            Header: 'Actions',
            accessor: 'ruleId',
            Cell: cellInfo => {
             return <Button onClick={()=>deleteRuleWithId(cellInfo?.value)}>Delete</Button>
            }
          }
    ],
    []
  )

  return (
    <Segment className="segment-class">
         <div style={{"textAlign":"right"}}>
            <Link to='/createrule'>
                <Button secondary onClick>Add Rule</Button>
            </Link>
        </div>
      {loading?<LoadingComponent />:<TableComponent columns={columns} data={data} />}
    </Segment>
  )
}

export default RuleList


