import React,{useState,useEffect} from 'react'
import { CSVReader } from 'react-papaparse'
import {Segment,Button, Header, Image, Modal } from 'semantic-ui-react'
import TableComponent from './TableComponent';
import _ from 'lodash';
import {saveReport} from '../models/ReportAPI';
import { toast } from 'react-toastify';

const getComponent = (type)=>{
   const components = {"workday":{"url":"/reports/workday","buttonText":"Upload Workday File","title":"Workday Data"},
                        "alight":{"url":"/reports/alight","buttonText":"Upload Alight File","title":"Alight Data"}}

    return components[type];
}

const makeData = (data)=>{
    let jsonArray = [];
    let columns = data[0].data
    delete data[0];
    data.forEach(function(value) {
        let jsonRow = {};
        value.data.forEach(function(dataValue,index){
            jsonRow[columns[index]] = dataValue;
        })
        jsonArray.push(jsonRow);
    });
   
    let tableColumns = columns.map((data)=>{
        return {
            Header: data,
            accessor: data,
        }
    })

    return {tableColumns,jsonArray};
}


const handleOpenDialog = (e) => {
    if (buttonRef.current) {
      buttonRef.current.open(e)
    }
  }
   
const handleOnError = (err, file, inputElem, reason) => {
    console.log(err)
}
 
const buttonRef = React.createRef()

const saveReportData = (file,url)=>{
    var formData = new FormData();
    formData.append("file", file);
    return saveReport(url,formData);
    
}

export default function UploadModal(props) {
    const [open, setOpen] = React.useState(false)
    const [data,setData] = React.useState([])
    const [columns,setColumnData] = React.useState([])
    const [file,setFile] = React.useState();
    const uploadTerms = getComponent(props.type);
    return (
      <Modal
        onClose={() => setOpen(false)}
        onOpen={() => setOpen(true)}
        open={open}
        trigger={<Button>{uploadTerms.buttonText}</Button>}
        className="uploadModal"
      >
        <Modal.Header>{uploadTerms.title}</Modal.Header>
        <Modal.Content image>
          <Modal.Description>
            <Header>Select File</Header>
            <CSVReader
                ref={buttonRef}
                onFileLoad={(data,file)=>{
                    console.log(file);
                    let tableData = makeData(data);
                    setColumnData(tableData.tableColumns);
                    setData(tableData.jsonArray);
                    setFile(file);
                }}
                onError={handleOnError}
                noClick
                noDrag
            >
                {({ file }) => (
                <aside
                    style={{
                    display: 'flex',
                    flexDirection: 'row',
                    marginBottom: 10
                    }}
                >
                    <button
                    type='button'
                    onClick={handleOpenDialog}
                    >
                    Browe file
                    </button>
                    <div>
                    {file && file.name}
                    </div>
                </aside>
                )}
            </CSVReader>
            {data.length>0 && <TableComponent columns={columns} data={data} />}
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          <Button color='black' onClick={() => setOpen(false)}>
            Cancel
          </Button>
          <Button
            content="Save Report"
            labelPosition='right'
            icon='checkmark'
            onClick={() => {
                saveReportData(file,uploadTerms.url).then((res)=>{
                    setOpen(false);
                    props.onClose()
                },error=>{
                    console.log(error);
                    toast.error("Failed to upload report")
                });
            }}
            positive
          />
        </Modal.Actions>
      </Modal>
    )
  }