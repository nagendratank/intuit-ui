import React,{useState,useEffect} from 'react'
import TableComponent from './TableComponent';
import {getReport} from '../models/ReportAPI';
import LoadingComponent from './LoadingComponent';
import {Segment } from 'semantic-ui-react'
import UploadComponent from './UploadComponent';
import { toast } from 'react-toastify';
import moment from 'moment-timezone';


function AlightReport() {
  const [loading, setLoading] = useState(true)
  const [data, setData] = useState([])
  
  const fetchData = async () => {
    setLoading(true);
    try {
        const result = await getReport("/reports/alight");
        setData(result.data);
        setLoading(false);
      } catch(err) {
        setLoading(false);
        toast.error("Failed to load alight report");
     }
  };

  const closeModal = ()=>{
    fetchData();
    toast.success("Report Published successfully");
  }

  useEffect(()=>{
      fetchData();
  },[])

  const columns = React.useMemo(
    () => [
        {
            Header: 'First Name',
            accessor: 'firstName',
          },
          {
            Header: 'Last Name',
            accessor: 'lastName',
          },
          {
            Header: 'Hire Date',
            accessor: 'hireDate',
            Cell: cellInfo => <span>{cellInfo.value?moment.tz(cellInfo.value,"Asia/Kolkata").format("MM/DD/YYYY"):null}</span>
          },{
            Header: 'Termination Date',
            accessor: 'terminationDate',
            Cell: cellInfo => <span>{cellInfo.value?moment.tz(cellInfo.value,"Asia/Kolkata").format("MM/DD/YYYY"):null}</span>
          }
    ],
    []
  )

  return (
    <Segment className="segment-class">
         <div style={{"textAlign":"right"}}>
            <UploadComponent columns={columns} type="alight" onClose={closeModal}/>
        </div>
      {loading?<LoadingComponent />:<TableComponent columns={columns} data={data} />}
    </Segment>
  )
}

export default AlightReport


