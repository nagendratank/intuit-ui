import React,{useState,useEffect} from 'react'
import TableComponent from './TableComponent';
import {getReport} from '../models/ReportAPI';
import LoadingComponent from './LoadingComponent';
import {Segment,Button, Header, Image, Modal } from 'semantic-ui-react'
import UploadComponent from './UploadComponent';
import { toast } from 'react-toastify';
import moment from 'moment-timezone';

function WorkDayReport() {
  const [loading, setLoading] = useState(true)
  const [data, setData] = useState([])
  
  const fetchData = async () => {
    setLoading(true);
    try {
        const result = await getReport("/reports/workday");
        setData(result.data);
        setLoading(false);
      } catch(err) {
        setLoading(false);
        toast.error("Failed to load workday report");
     }
  };

  const closeModal = ()=>{
    fetchData();
    toast.success("Report Published successfully");
  }

  useEffect(()=>{
      fetchData();
  },[])

  const columns = React.useMemo(
    () => [
        {
            Header: 'First Name',
            accessor: 'firstName',
          },
          {
            Header: 'Last Name',
            accessor: 'lastName',
          },
          {
            Header: 'Employment Type',
            accessor: 'employmentType',
            backGroundColor: cellInfo => {
              return {style:{backgroundColor:cellInfo?.row?.original?.rule?.condition?.indexOf("employment_type")>=0 && cellInfo?.row?.original?.rule?.color}}
            }
          },
          {
            Header: 'Hire Date',
            accessor: 'hireDate',
            Cell: cellInfo => <span>{cellInfo.value?moment.tz(cellInfo.value,"Asia/Kolkata").format("MM/DD/YYYY"):null}</span>,
            backGroundColor: cellInfo => {
              return {style:{backgroundColor:cellInfo?.row?.original?.rule?.condition?.indexOf("hire_date")>=0 && cellInfo?.row?.original?.rule?.color}}
            }
          },{
            Header: 'Termination Date',
            accessor: 'terminationDate',
            Cell: cellInfo => <span>{cellInfo.value?moment.tz(cellInfo.value,"Asia/Kolkata").format("MM/DD/YYYY"):null}</span>
          },
          {
            Header: 'Termination Status',
            accessor: 'terminationStatus',
            Cell: cellInfo => <span>{cellInfo.value?'True':'False'}</span>,
            backGroundColor: cellInfo => {
              console.log(cellInfo)
              return {style:{backgroundColor:cellInfo?.row?.original?.rule?.condition?.indexOf("termination_status")>=0 && cellInfo?.row?.original?.rule?.color}}
            }
          },
          {
            Header: 'Termination Reason',
            accessor: 'terminationReason',
            backGroundColor: cellInfo => {
              return {style:{backgroundColor:cellInfo?.row?.original?.rule?.condition?.indexOf("termination_reason")>=0 && cellInfo?.row?.original?.rule?.color}}
            }
          },
          {
            Header: 'Leave Status',
            accessor: 'leaveStatus',
            backGroundColor: cellInfo => {
              return {style:{backgroundColor:cellInfo?.row?.original?.rule?.condition?.indexOf("leave_status")>=0 && cellInfo?.row?.original?.rule?.color}}
            }
          },
          {
            Header: 'Leave Type',
            accessor: 'leaveType',
            backGroundColor: cellInfo => {
              return {style:{backgroundColor:cellInfo?.row?.original?.rule?.condition?.indexOf("leave_type")>=0 && cellInfo?.row?.original?.rule?.color}}
            }
          }
    ],
    []
  )

  return (
    <Segment className="segment-class">
        <div style={{"textAlign":"right"}}>
            <UploadComponent columns={columns} type="workday" onClose={closeModal}/>
        </div>
     
      {loading?<LoadingComponent />:<TableComponent columns={columns} data={data} />}
    </Segment>
  )
}

export default WorkDayReport


