
import React from 'react'
import { Icon, Table } from 'semantic-ui-react'
import { useTable } from 'react-table'

function TableComponent({ columns, data }) {
    // Use the state and functions returned from useTable to build your UI
    const {
      getTableProps,
      getTableBodyProps,
      headerGroups,
      rows,
      prepareRow,
    } = useTable({
      columns,
      data,
    })
  
    // Render the UI for your table
    return (
      <Table {...getTableProps()}>
        <Table.Header>
          {headerGroups.map(headerGroup => (
            <Table.Row {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map(column => (
                <Table.HeaderCell {...column.getHeaderProps()}>{column.render('Header')}</Table.HeaderCell>
              ))}
            </Table.Row>
          ))}
       </Table.Header>
       
        <Table.Body {...getTableBodyProps()}>
        {rows.length===0 &&
            <tr>
                <td colspan={columns.length} className="noDataFound">
                    No data found
                </td>
            </tr>
        }
          {rows.map((row, i) => {
            prepareRow(row)
            return (
              <Table.Row {...row.getRowProps()}>
                {row.cells.map(cell => {
                  return <Table.Cell {...cell.getCellProps()} { ...cell.column.backGroundColor?.(cell)}>{cell.render('Cell')}</Table.Cell>
                })}
              </Table.Row>
            )
          })}
        </Table.Body>
      </Table>
    )
  }
  

  export default TableComponent;